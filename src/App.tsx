import React from 'react';
import './styles/App.css';

import Layout from './layout/Index';

const App: React.FC = () => <Layout />;

export default App;
