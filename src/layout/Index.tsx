import React from 'react';

import Header from './header/Header';
import Message from './welcome/Message';
import MainContent from './mainContaint/MainContent';

const Index: React.FC = () => (
  <div>
    <Header />
    <Message />
    <MainContent />
  </div>
);

export default Index;
