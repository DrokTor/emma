import React, { useState } from 'react';
import { useMediaQuery } from 'react-responsive';
import { Collapse } from 'react-collapse';

import Menu from './Menu';
import MenuRight from './MenuRight';
import img from '../../assets/emma_logo.svg';

interface MenuType{
    menu: boolean;
    left: boolean;
    right: boolean;
}

const Header: React.FC = () => {
  const isMobileOrTab = useMediaQuery({ query: '(max-width: 1199px)' });
  const isTab = useMediaQuery({ query: '(min-width: 768px)' });
  const [isOpen, setIsOpen] = useState<MenuType>({ menu: false, left: true, right: false });

  return (
    <header className="row m-0 p-3">
      {isMobileOrTab && (
        <button className="btn p-0 d-flex align-items-center" type="button" onClick={(): void => setIsOpen({ ...isOpen, menu: !isOpen.menu })}>
          <i className="fa fa-bars fa-2x text--primary mr-2" />
          {isTab && <span className="pt-1">Menu</span>}
        </button>
      )}
      <img src={img} alt="emma_logo" className="mx-auto mx-xl-0" />
      {isMobileOrTab && (
        <button className="btn p-0 d-flex align-items-center" type="button">
          {isTab && <span className="pt-1">Cart</span>}
          <i className="fa fa-shopping-cart fa-2x text--primary ml-2" />
        </button>
      )}
      {isMobileOrTab && (
        <Collapse theme={{ collapse: 'ReactCollapse--collapse col-12 text-center', content: 'ReactCollapse--content' }} isOpened={isOpen.menu}>
          <Collapse theme={{ collapse: 'ReactCollapse--collapse col-12', content: 'ReactCollapse--content' }} isOpened={isOpen.left}>
            <Menu />
          </Collapse>
          <hr className="col-md-9" />
          <Collapse theme={{ collapse: 'ReactCollapse--collapse col-12', content: 'ReactCollapse--content' }} isOpened={isOpen.right}>
            <MenuRight />
          </Collapse>
          <button className="btn p-0" type="button" onClick={(): void => setIsOpen({ menu: true, left: !isOpen.left, right: !isOpen.right })}>
            <i className={`fa fa-chevron-${isOpen.left ? 'up' : 'down'} fa-1x text--primary`} />
          </button>
        </Collapse>
      )}
      {!isMobileOrTab && (
        <>
          <Menu />
          <MenuRight />
        </>
      )}
    </header>
  );
};

export default Header;
