import React from 'react';


const Menu: React.FC = () => (
  <nav className="col-12 pt-3 pt-xl-0 col-xl-6 px-xl-0">
    <ul className="menu m-0 pl-0 pl-xl-2 text-md-center text-xl-left">
      <li className="menu__item p-3 py-md-2 p-xl-0 ml-xl-3">
        Mattresses
        <i className="icon-menu--down" />
      </li>
      <li className="menu__item p-3 py-md-2 p-xl-0 ml-xl-3">
        Accessories
        <i className="icon-menu--down" />
      </li>
      <li className="menu__item p-3 py-md-2 p-xl-0 ml-xl-3">Bedframe</li>
      <li className="menu__item p-3 py-md-2 p-xl-0 ml-xl-3">Bundles</li>
      <li className="menu__item p-3 py-md-2 p-xl-0 ml-xl-3">Kids</li>
      <li className="menu__item p-3 py-md-2 p-xl-0 ml-xl-3">Dogs</li>
    </ul>
  </nav>
);

export default Menu;
