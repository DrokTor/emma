import React from 'react';


const Menu: React.FC = () => (
  <nav className="col-12 col-xl-4 ml-xl-auto  px-xl-1">
    <ul className="menu m-0 pl-0 pl-xl-2 text-md-center text-xl-right">
      <li className="menu__item p-3 py-md-2 p-xl-0 ml-xl-3">
        English
        <i className="icon-menu--down" />
      </li>
      <li className="menu__item p-3 py-md-2 p-xl-0 ml-xl-3">Awards</li>
      <li className="menu__item p-3 py-md-2 p-xl-0 ml-xl-3">Reviews</li>
      <li className="menu__item p-3 py-md-2 p-xl-0 ml-xl-3">Help</li>
      <li className="menu__item p-3 py-md-2 p-xl-0 ml-xl-3 d-none d-xl-inline">
        <i className="fa fa-shopping-cart mr-2" />
        Cart
      </li>
    </ul>
  </nav>
);

export default Menu;
