import React from 'react';
import Unique from 'unique-string';

type StatType = {
  title: string;
  desc: string;
}

const statsList: StatType[] = [
  { title: '56', desc: 'Languages spoken' },
  { title: '203', desc: 'Employees' },
  { title: '24', desc: 'Average age' },
  { title: '51', desc: 'Countries where people come from' },
];

const Stats: React.FC = () => (
  <section className="figures row ">
    {
      statsList.map((st: StatType): React.ReactNode => (
        <div className="col-6 mb-3 pr-md-4 col-lg-3" key={Unique()}>
          <h1 className="mb-0">{st.title}</h1>
          <span>{st.desc}</span>
        </div>
      ))
    }
  </section>
);

export default Stats;
