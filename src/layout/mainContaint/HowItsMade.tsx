import React from 'react';


const HowItsMade: React.FC = () => (
  <article className="how-its-made p-lg-4">
    <h2 className="mb-md-0">How our mattress is made</h2>
    <p className="">
      get to know our mattress design and how its made etc. brief info on where its made
    </p>
    <button type="button" className="btn btn--primary py-2-5 px-4-5">
      See how it&apos;s made
    </button>
  </article>
);

export default HowItsMade;
