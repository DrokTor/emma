import React from 'react';


const EmmaNetherlands: React.FC = () => (
  <article className="mt-5 mb-5">
    <h1>Emma in the Netherlands</h1>
    <p className="">
      why we serve the specific country and why these customers are important to us etc.
      Mariska was de eerste Nederlandse klant van Emma. En nog voordat de 100 dagen testslapen
      voorbij waren, was Mariska zo enthousiast dat ze Emma naar Nederland haalde.
      Daarom kun jij nu slapen op het matras van je dromen. Gegarandeerd.
      <br />
      <br />
      Nu zorgen Mariska en haar team ervoor dat iedereen in Nederland van het Emma matras
      kan genieten.
      Bel gerust op om kennis te maken of bestel Emma direct!
    </p>
  </article>
);

export default EmmaNetherlands;
