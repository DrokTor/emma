import React from 'react';
import { useMediaQuery } from 'react-responsive';

import OurStory from './OurStory';
import EmmaNetherlands from './EmmaNetherlands';
import OurTeam from './OurTeam';
import HowItsMade from './HowItsMade';
import Stats from './Stats';
import JoinUs from './JoinUs';
import Awards from './Awards';

const MainContent: React.FC = () => {
  const isMobile = useMediaQuery({ query: '(max-width: 767px)' });

  return (
    <section className="row m-0 pt-xl-5 pb-md-5">
      <section className="col-md-7 col-12 ml-xl-auto  pr-xl-5">
        <OurStory />
        <EmmaNetherlands />
        <HowItsMade />
        {isMobile && <Awards />}
        <OurTeam />
        <Stats />
        <JoinUs />
      </section>
      {!isMobile && <Awards />}
    </section>
  );
};

export default MainContent;
