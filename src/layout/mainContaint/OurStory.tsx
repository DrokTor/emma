import React from 'react';


const OurStory: React.FC = () => (
  <article>
    <h1>Our Story</h1>
    <p className="">
      Emma started in Germany at the end of December 2015 driven by wanting
      to take the complexity out of purchasing a new mattress. The aim was
      to develop a mattress that is suitable for a variety of body and sleep types
      using only high quality materials to ensure a perfect balance for support.
      Today, we are in over 10 countries and are the most awarded mattress in Europe
      thanks to our focus on providing our customers across the world with the a truly
      worry free sleep.
    </p>
  </article>
);

export default OurStory;
