import React, { useState } from 'react';
import { useMediaQuery } from 'react-responsive';
import { Collapse } from 'react-collapse';
import Unique from 'unique-string';


type AwardItem = {
  name: string;
  desc: string;
}

interface AwardType{
  title: string;
  list: AwardItem[];
}

interface RenderAwardPropsType{
  awards: AwardType;
  isMobile?: boolean;
}

const RenderAwards: React.FC<RenderAwardPropsType> = ({
  awards,
  isMobile = false,
}: RenderAwardPropsType) => (
  <div className="row m-0 pb-5 pl-md-1 pl-xl-0">
    <div className="col-2 col-sm-1 col-md-3 pr-0 pl-2 pl-sm-3">
      <div className="h-100 d-flex flex-column align-items-center">
        {!isMobile && <h2 className="text--secondary mb-3">{awards.title}</h2>}
        <div className="bar h-100 mt-4 mt-md-2 bg--secondary" />
      </div>
    </div>
    <div className="col-10 col-sm-11 col-md-9 pt-3 pr-xl-0 pl-sm-5 pl-md-3">
      {
        awards.list.map((aw: AwardItem): React.ReactNode => (
          <div className="mt-4-5" key={Unique()}>
            <div className="circle bg--secondary " />
            <h6>{aw.name}</h6>
            <p>{aw.desc}</p>
          </div>
        ))
      }
    </div>
  </div>
);

const Awards2018: AwardType = {
  title: '2018',
  list: [
    { name: 'Tech5/TNW Award', desc: 'Awarded as the top 1 fastest growin startup in Europe.' },
    { name: 'Consumentenbond Consumer’s Assiciation, NL', desc: 'Ranked number 1 for the best mattress in mattress test.' },
    { name: 'Which? Consumer’s Assiciation, UK', desc: 'Ranked number 1 for the best tested mattress in the UK.' },
    { name: 'Elu Produit de L’annee, FR', desc: 'Received the highest score out of 24 brands and elected ‘Product of the year 2018’.' },
    { name: 'Kununu, Germany', desc: 'Rated as top 10 best companies to work at with highest employee satisfaction.' },
  ],
};

const Awards2017: AwardType = {
  title: '2017',
  list: [
    { name: 'Tech5/TNW Award', desc: 'Awarded as the top 1 fastest growin startup in Europe.' },
    { name: 'Consumentenbond Consumer’s Assiciation, NL', desc: 'Ranked number 1 for the best mattress in mattress test.' },
    { name: 'Which? Consumer’s Assiciation, UK', desc: 'Ranked number 1 for the best tested mattress in the UK.' },
  ],
};

const Awards: React.FC = () => {
  const isMobile = useMediaQuery({ query: '(max-width: 767px)' });
  const [isOpen, setIsOpen] = useState<boolean>(true);
  const [isOpen2, setIsOpen2] = useState<boolean>(false);

  return (
    <aside className="col-12 col-md-5 col-xl-3 pl-0 pr-0 pr-md-4 pr-xl-2 mr-auto">
      <h3 className="pl-md-3 pl-lg-4 pl-xl-0 mb-4 mb-md-3 mb-xl-4 mt-3 mt-md-1 mt-xl-3">Awards and Accolades</h3>
      <div className="row">
        {isMobile && (
          <>
            {/* <div className="row m-0 col-12 p-0">
            </div> */}
            {/* <h6 className="col-2 px-0 text-center"> */}
            <h6 className="col-12 pr-1">
              2018
              <button className="btn float-right ml-auto" type="button" onClick={(): void => setIsOpen(!isOpen)}>
                <i className={`fa ${isOpen ? 'fa-chevron-up' : 'fa-chevron-down'}`} />
              </button>
            </h6>
            <Collapse isOpened={isOpen} theme={{ collapse: 'ReactCollapse--collapse col-12 pl-0', content: 'ReactCollapse--content' }}>
              <RenderAwards awards={Awards2018} isMobile={isMobile} />
            </Collapse>
            <h6 className="col-12 pr-1">
              2017
              <button className="btn float-right" type="button" onClick={(): void => setIsOpen2(!isOpen2)}>
                <i className={`fa ${isOpen2 ? 'fa-chevron-up' : 'fa-chevron-down'}`} />
              </button>
            </h6>
            <Collapse isOpened={isOpen2} theme={{ collapse: 'ReactCollapse--collapse col-12 pl-0', content: 'ReactCollapse--content' }}>
              <RenderAwards awards={Awards2017} isMobile={isMobile} />
            </Collapse>
          </>
        )}
        {
          !isMobile && (
            <>
              <RenderAwards awards={Awards2018} />
              <RenderAwards awards={Awards2017} />
            </>
          )
        }
      </div>
    </aside>
  );
};

export default Awards;
