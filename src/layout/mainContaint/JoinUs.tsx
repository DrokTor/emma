import React from 'react';


const JoinUs: React.FC = () => (
  <article className="mt-5 mb-5">
    <h1>Join Our Team</h1>
    <p className=" ">
      we are always looking for good people. if youre an intern youre also welcome,
      if youre looking for the next step in your career yeah check out our jobs.
      <br />
      <br />
      *somewhere mention info about Emma being part of Bettzeit
    </p>
    <button type="button" className="mt-3 btn btn--secondary col-12 col-md-10 col-lg-6 py-2-5">
      Check our job openings
    </button>
  </article>
);

export default JoinUs;
