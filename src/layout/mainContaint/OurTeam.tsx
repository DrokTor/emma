import React from 'react';
import { useMediaQuery } from 'react-responsive';

import profile from '../../assets/profile.png';

const OurTeam: React.FC = () => {
  const isMobile = useMediaQuery({ query: '(max-width: 767px)' });

  return (
    <article className="mt-5 mb-3 mb-lg-5">
      {isMobile && (
        <div className="text-center mb-3">
          <img src={profile} alt="mariska-netherlands" />
        </div>
      )}
      <h1>Our Team</h1>
      <div className="row mb-1 mb-lg-5 mt-3">
        {!isMobile && (
          <div className="col-md-5 col-lg-3">
            <img src={profile} alt="mariska-netherlands" />
          </div>
        )}
        <p className="col-md-7 col-lg-9">
          Mariska is the cofounder of Emma Netherlands and this is a description of what she does
          and why she brought Emma to the Netherlands and connect it a bit to the information
          above. A bit more about her to make it sound personable and approachable and to show
          that there is a face behind our brand.
        </p>
      </div>
      <h6>
        Emma is based in Frankfurt, Germany and our team is very international and we are open
        to all cultures. good working envirnoment very open with flat hierarchies and what not.
      </h6>
    </article>
  );
};

export default OurTeam;
