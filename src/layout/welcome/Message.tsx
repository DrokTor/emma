import React from 'react';
import { useMediaQuery } from 'react-responsive';

import img from '../../assets/logo/claim_S@4x.svg';
import imgSmall from '../../assets/logo/claim_S.png';

const Message: React.FC = () => {
  const isMobile = useMediaQuery({ query: '(max-width: 767px)' });
  const isTablet = useMediaQuery({ query: '(max-width: 1200px)' });

  return (
    <section className="welcome-message mx-auto mb-5 pt-4 pt-lg-0">
      <div className="row m-0 d-flex flex-column align-items-center">
        <img src={isMobile ? imgSmall : img} width={isTablet && !isMobile ? 474 : ''} alt="sleep-happy" />
        <div className="row m-0 pt-3 pt-md-3 pt-lg-5">
          <p className="col-md-8 col-lg-6 pl-4 pr-4 mx-auto text-center">
            our aim is to provide you with a better sleep
            (mission statement or some sort of company purpose
            that we believe in and work towards/ our values?!)
          </p>
        </div>
      </div>
    </section>
  );
};

export default Message;
